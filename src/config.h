#ifndef I3_EASYFOCUS_CONFIG
#define I3_EASYFOCUS_CONFIG

#define EXIT_KEYSYM XK_Escape
#define LABEL_KEYSYMS { XK_1, XK_2, XK_3, XK_q, XK_w, XK_e, XK_a, XK_s, XK_d, XK_z, XK_x, XK_c, XK_4, XK_5, XK_6, XK_7, XK_8, XK_9, XK_0, XK_r, XK_t, XK_y, XK_u, XK_i, XK_o, XK_p, XK_f, XK_g, XK_h, XK_j, XK_k, XK_l, XK_v, XK_b, XK_n, XK_m }

#define XCB_DEFAULT_FONT_NAME "-Misc-Fixed-Bold-R-Normal--18-120-100-100-C-90-ISO10646-1"

#endif
